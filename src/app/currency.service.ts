import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  constructor(private http: HttpClient) { }


  public async convert(amount: number) {
    const rate:number = await this.getExchangeRate();
    return rate * amount;
  }
  
  private async getExchangeRate(): Promise<number> {
    const data = await this.executeHTTPCall();
    const rate = this.getRateFromHTTPResult(data);
    return rate;
  }

  private getRateFromHTTPResult(data): number {
    let result: number = 0;
    const ratesName = 'rates';
    const apiRates = Object.getOwnPropertyNames(data[ratesName]);
    apiRates.forEach(el => {
      if (el === 'GBP') {
        result = data[ratesName][el];         
      }
    });
    return result;
  }

  private executeHTTPCall() {
    return new Promise(resolve => {
      this.http.get(environment.apiUrl).subscribe(data => {
       resolve(data);
       }, err => {
         console.log(err);
      });
    });
  }

}
