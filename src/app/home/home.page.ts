import { Component } from '@angular/core';
import { CurrencyService } from '../currency.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private euros: number;
  private pounds: any;

  constructor(public currencyService: CurrencyService) {}

  public  calculate() {
    this.currencyService.convert(this.euros).then(result => {
      console.log('result ' + result);
      this.pounds = result;
    });
  }
}
